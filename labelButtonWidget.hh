#ifndef LABELBUTTONWIDGET_HH
#define LABELBUTTONWIDGET_HH

#include "hlabelbuttoncombo.hh"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>

class labelButtonWidget : public QWidget
{
    Q_OBJECT
public:
    labelButtonWidget(QString btnTxt, QWidget* parent);
    void removeProgram(HLabelButtonCombo* widget);
    void addProgram(QString programName, int index);
signals:
    void programRemoved(QString programName);
private:
    QVBoxLayout* mainLayout_;
    QString btnTxt_;
};

#endif // LABELBUTTONWIDGET_HH
