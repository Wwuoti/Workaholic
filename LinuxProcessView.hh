#ifndef LINUXPROCESSVIEW_HH
#define LINUXPROCESSVIEW_HH

#include <QStringList>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/scrnsaver.h>

//these have to be undefined since QT also uses them
//and thus the code won't compile otherwise
#undef Bool
#undef CursorShape
#undef Expose
#undef KeyPress
#undef KeyRelease
#undef FocusIn
#undef FocusOut
#undef FontChange
#undef None
#undef Status
#undef Unsorted

class LinuxProcessView{

public:
    LinuxProcessView();
    ~LinuxProcessView();
        
    //List all the processes and the commands used to start them   
    void printProcesses();
    std::map <int, std::string> getPIDMap();
    //Makes a QStringList from the map processList_ to be
    //used in the GUI
    QStringList getProcessList();

    //fetches data related to specified window atom
    void getWindowAtom(const char* windowAtom, 
            Display* display, Window window,
            unsigned long &numItems, long*& array);
    bool isUserActive();
private:    

    //updates processList_ with Linux process pids  
    //matched with commands used to exec them
    void listProcesses();

    //checks if given window is active
    bool isActiveWindow(int givenPID);    
    std::map <int, std::string> processList_;

    //connection to the display of the OS
    Display* maindisp_;
}; 


#endif
