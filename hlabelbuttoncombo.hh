#ifndef HLABELBUTTONCOMBO_HH
#define HLABELBUTTONCOMBO_HH

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>

class HLabelButtonCombo : public QWidget
{
    Q_OBJECT
public:
    HLabelButtonCombo(QString programName, QString btnTxt, QWidget* parent);
    void buttonClicked();
    QString getProgramName();
signals:
    void clicked(HLabelButtonCombo* widget);
private:
    QString programName_;
};

#endif // HLABELBUTTONCOMBO_HH
