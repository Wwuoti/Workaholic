#include "sidebaritem.hh"
#include <QDebug>
SidebarItem::SidebarItem(QWidget* parent,
                         const QString iconPath, const QString actionName)
    : QToolButton(parent){
    this->setIcon(QIcon(iconPath));
    this->setText(actionName);
    this->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}

//bool SidebarItem::event(QEvent *event){
//    //if (event->type() == QEvent::Paint){
//       return QToolButton::event(event);
//    //}
//    //return true;
//
//}
