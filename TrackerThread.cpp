#include "TrackerThread.hh"

TrackerThread::TrackerThread(QObject *parent)
    : QThread(parent){
    ProcessView_ = new LinuxProcessView;
    maindisp_ = XOpenDisplay(NULL);
     
    
    }
TrackerThread::~TrackerThread(){
    trackerDb_->addTimestamp(NULL);
    XCloseDisplay(maindisp_);
}

//Execs a command given as 'cmd' parameter  and returns its output
std::string TrackerThread::exec(const char* cmd) {
    char buffer[128];
    std::string result = "";

    //create pipe to save command output
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

void TrackerThread::trackWindow (){
    //TODO: mouse and keyboard tracking
    std::map<int, std::string> PIDList = ProcessView_->getPIDMap();
    emit PIDMapChanged(); 
    //get the root window of the main display 
    Window rootWindow = DefaultRootWindow(maindisp_);
    //search for the currently active window
    const char* atom = "_NET_ACTIVE_WINDOW";
    unsigned long numActiveWindow;
    long *activeWindow;
    ProcessView_->getWindowAtom(atom,
            maindisp_, rootWindow, numActiveWindow, activeWindow);
    //get the pid of the currently active window
    unsigned long PIDItems;
    long* PID_arr;
    ProcessView_->getWindowAtom("_NET_WM_PID", maindisp_, activeWindow[0], PIDItems, PID_arr);
    int newWindowPID;
    if(PID_arr == nullptr){
        newWindowPID = 0;
    }else{
        newWindowPID = (int) PID_arr[0]; 
    }
    //check if window's PID is different than previously and that the PID 
    //exists in the list 
    if ( PIDList.find(newWindowPID) != PIDList.end()){

        QString processName = QString::fromStdString(PIDList[newWindowPID]);
        //check if user has defined the program to be tracked 
        //if so, then add a timestamp with its name
        if(trackedPrograms_.find(processName) != trackedPrograms_.end() 
                && newWindowPID != currentWindowPID_
                && ProcessView_->isUserActive()){
            std::cout <<"Program changed to " <<  processName.toStdString() << std::endl;
            istracked_ = true;
            trackerDb_->addTimestamp(processName);
        //otherwise add just a timestamp with NULL as program name
        //This means that unproductive time can be checked as duration
        //from last null to next tracked program timestamp
        currentWindowPID_ = newWindowPID;
        }else if (istracked_ && 
                (trackedPrograms_.find(processName) == trackedPrograms_.end() 
                || !ProcessView_->isUserActive())){
            trackerDb_->addTimestamp(NULL);
            istracked_ = false;
            currentWindowPID_ = 999999;
        }
    }
    //Data fetched with Xlib must be freed manually
    XFree(activeWindow);
    XFree(PID_arr);

    
}


void TrackerThread::run(){

    //Connect to database containing process timestamps
    //These cannot be in the constructor, as it's run by the main thread
    QString path = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) 
        + "/Workaholic/tracked.db";
        trackerDb_ = new DbManager(path, "TrackerThread");
    trackedPrograms_ = trackerDb_->loadTrackables();
    std::cout << "Loaded following programs from database: " << std::endl;
    for (auto program : trackedPrograms_){
        std::cout << program.toStdString() << std::endl;
    }
    emit trackedProgramsOnStart(trackedPrograms_);
    connect(this, &TrackerThread::newTrackable, trackerDb_, &DbManager::saveTrackable, Qt::QueuedConnection);
    connect(this, &TrackerThread::deleteTrackable, trackerDb_, &DbManager::removeTrackable, Qt::QueuedConnection);
    //don't waste thread time, track window only once per second
    timer_ = new QTimer;    
    connect(timer_, &QTimer::timeout, this, &TrackerThread::trackWindow, Qt::DirectConnection);
    timer_->start(1000);    

    //allows thread to receive signals
    QThread::exec();
}

void TrackerThread::addNewTrackable(QString programName){
    if (trackedPrograms_.find(programName) == trackedPrograms_.end()){
        trackedPrograms_.insert(programName);
        emit newTrackable(programName);
        std::cout << "Currently added programs:" << std::endl;
        for (auto program : trackedPrograms_){
            std::cout << program.toStdString() << std::endl;
        }
    }
}

void TrackerThread::removeTrackable(QString programName)
{
    auto it = trackedPrograms_.find(programName);
    if (it != trackedPrograms_.end()){
        trackedPrograms_.erase(it);
        emit deleteTrackable(programName);
        std::cout << "Currently added programs:" << std::endl;
        for (auto program : trackedPrograms_){
            std::cout << program.toStdString() << std::endl;
        }
    }
}
