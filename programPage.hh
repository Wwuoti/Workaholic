#ifndef PROGRAMPAGE_HH
#define PROGRAMPAGE_HH

#include "TrackerThread.hh"
#include "LinuxProcessView.hh"
#include "labelButtonWidget.hh"

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QDebug>
#include <QScrollArea>
#include <QObject>
#include <QVector>

class programPage : public QWidget {
    Q_OBJECT

public:
    programPage(QWidget *parent);
    ~programPage();

    void setupPage();
public slots:
    void updateProcessBox();
    void removeProgram(QString programName);
    void addProgramsOnStart(const QSet<QString> programs);
signals:
    void newTrackable(QString trackable);
    void removeTrackable(QString trackable);
private:
    void sendTrackable();
    void addProgramLabel(QString programName);
    QComboBox* processBox_;
    QStringList QProcessList_;

    QVBoxLayout* mainLayout_;
    labelButtonWidget* programWidget_;
    QVector<QString> trackedPrograms_;

    QPushButton* AddButton_;

    LinuxProcessView* LinuxProcesses_;
};

#endif
