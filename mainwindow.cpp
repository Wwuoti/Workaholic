#include <QFrame>
#include <QHBoxLayout>
#include <QDesktopWidget>

#include "mainwindow.hh"


MainWindow::MainWindow(QWidget *parent): QMainWindow(parent){
    MainWindow::setWindowFlag(Qt::Dialog, true);
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    setupLayout();
            
}

MainWindow::~MainWindow(){
}

void MainWindow::setupLayout(){
    QFrame* frame = new QFrame(this);
    QHBoxLayout* hlayout = new QHBoxLayout(frame);
    hlayout->setSizeConstraint(QLayout::SetMinimumSize);
    //sidebar to switch between program adding and data analyzing
    sidebar_ = new SideBar(this);
    hlayout->addWidget(sidebar_);
    QAction* programAction = sidebar_->
            addAction(QIcon(":/icons/puzzle.png"), "Programs");
    sidebar_->setIconSize(QSize(60,60));
    QAction* statisticsAction =
            sidebar_->addAction(QIcon(":/icons/statistics.png"), "Statistics");
    sidebar_->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionList_.push_back(programAction);
    actionList_.push_back(statisticsAction);
    connect(sidebar_, &SideBar::actionTriggered,
            this, &MainWindow::changeNavstackIndex);
    //downsize the huge sidelist, additional 10 pixels to prevent scrollbar
    //sidelist_->setMinimumWidth(sidelist_->sizeHintForColumn(0)+10);

    programPage* programpage = new programPage(this);
    statisticPage* statisticpage = new statisticPage(this);

    //vertical layout for displaying other controls 
    QVBoxLayout* vlayout = new QVBoxLayout();
    hlayout->addLayout(vlayout);
    
    //pages for navigation around different features
    navstack_ = new QStackedWidget(); 
    navstack_->addWidget(programpage);
    navstack_->addWidget(statisticpage);
    //connect(sidelist_, &QListWidget::currentRowChanged,
    //      navstack_, &QStackedWidget::setCurrentIndex);
    vlayout->addWidget(navstack_);
    
    setCentralWidget(frame);

    //layout for list and labels 
    QDialogButtonBox* buttonLayout = new QDialogButtonBox(QDialogButtonBox::Ok 
            | QDialogButtonBox::Close );
    vlayout->addWidget(buttonLayout);

    //connect buttons to matched actions
    connect(buttonLayout, SIGNAL(rejected()), this, SLOT(close()));
    connect(buttonLayout, &QDialogButtonBox::accepted, this, &MainWindow::toTray);;
    
    trayIcon_ = new QSystemTrayIcon(QIcon(QStandardPaths::writableLocation(
                    QStandardPaths::HomeLocation) + "/Workaholic/icon_tray.png"), this); 
    connect(trayIcon_, &QSystemTrayIcon::activated, this, &MainWindow::fromTray);

    
    trackerThread_= new TrackerThread;  
    //connect Add-button to function adding trackable programs
    connect(programpage, &programPage::newTrackable, trackerThread_, &TrackerThread::addNewTrackable);

    connect(trackerThread_, &TrackerThread::trackedProgramsOnStart, programpage,
            &programPage::addProgramsOnStart);

    connect(programpage, &programPage::removeTrackable, trackerThread_, &TrackerThread::removeTrackable);

    connect(trackerThread_, &TrackerThread::PIDMapChanged, 
            programpage, &programPage::updateProcessBox);
    trackerThread_->start();
    resize(QDesktopWidget().availableGeometry(this).size() * 0.7);

}

void MainWindow::toTray(){
    trayIcon_->show();
    this->hide();
}

void MainWindow::fromTray(){
    this->show();
    trayIcon_->hide();
}

QString MainWindow::runPath(){
    return programPath_;
}

void MainWindow::changeNavstackIndex(QAction* action){
   int index = actionList_.indexOf(action);
   navstack_->setCurrentIndex(index);
}
