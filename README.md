# Workaholic

Workaholic is a program to track your working efficiency based on the time you spend using your program. Currently a work in progress and only available on Linux.

## Getting started

Use these instructions to make a test build on Linux.

### Prerequisites

- Qt5
- Qt5 charts
- C++17 (Needs GCC 7=>, might want to edit .pro file flags a bit since earlier C++ is probably fine)
- A window manager supporting Extended Window Manager Hints
- X11 dev library
- sqlite3 dev library

#### Installing prequisities

Depending on your distribution of Linux and installation method of Qt if you are missing Qt5 charts, the easiest way to get it might be reinstalling Qt5 altogether.
On Debian based systems to get the required X11 libraries execute sudo apt-get install libxss-dev and to get the required sqlite 3 libraries execute
sudo apt-get install libsqlite3-dev

### Building

```
mkdir Workaholic
cd Workaholic
git clone https://gitlab.com/Wwuoti/Workaholic/tree/master
qmake
make
```

### Running

Currently the program doesn't include an installer to create any desktop files, so simply run it from the build directory in a terminal:
```
./summerProject
```

### Program structure

The current build of the program only contains one window with minimal options, showing a list of other currently open windows. When a window is added to be tracked, DbManager::saveTrackable creates an entry into a 'savedPrograms' table in the database 'tracked.db', which is created when the program is started.

After startup another thread is started to run the TrackerThread-module, which calls trackWindow() every second. The active window is compared to tracked windows and user activity. If the window is something else than what it should be, OR the user is inactive a NULL entry will be written to the 'timestamp' table in the database along with a timestamp. If the user is active and a tracked program is in the foreground, a timestamp along with the program name will be written into the table.  
