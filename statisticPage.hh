#ifndef STATISTICPAGE_HH
#define STATISTICPAGE_HH

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QBoxLayout>
#include <QLabel>
#include <QtCharts/QChartView>
#include <QtCharts/QPercentBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtWidgets/QApplication>
#include <QStandardPaths>
#include <QScrollArea>
#include <QSizePolicy>
#include <QTableWidget>
#include <QHeaderView>
#include <QDesktopWidget>

#include <vector>
#include "DbManager.hh"
QT_CHARTS_USE_NAMESPACE

class statisticPage : public QWidget{
    Q_OBJECT

public:
    statisticPage(QWidget *parent);
    ~statisticPage();
signals:
    void needStats();
    void newWeek(QDate date);
private:
    void updateGraph(const QString dateString_);
    void addWeekTable(QDate date);
    void weekChanged(QString week);
    void yearChanged(QString year);
    void doubleClick(QTableWidgetItem* item);
    QDate beginningOfWeek(int week,
                          int year = QDate::currentDate().year());
    QComboBox* weekSelector_;
    QComboBox* yearSelector_;
    DbManager* statisticDb_;
    QVBoxLayout* graphLayout_;
    QChartView *chartView_ = nullptr;
    QScrollArea* chartscroller_;
    QTableWidget* weekTable_;
};

#endif
