#ifndef SIDEBAR_HH
#define SIDEBAR_HH
#include <QToolBar>
#include <QPainter>
#include <QPaintEvent>

class SideBar : public QToolBar
{
public:
    SideBar(QWidget* parent = nullptr);
protected:
    void paintEvent(QPaintEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

    std::vector<QAction*> actionList_;
};

#endif // SIDEBAR_HH
