﻿#include "sidebar.hh"
#include <QDebug>
SideBar::SideBar(QWidget* parent) : QToolBar(parent){
    this->setOrientation(Qt::Vertical);
    setAttribute(Qt::WA_Hover, true);
    setFocusPolicy(Qt::NoFocus);
    setMouseTracking(true); // Needed for hover events
}




void SideBar::paintEvent(QPaintEvent *event){
   QPainter painter(this);
   //painter.fillRect(event->rect(), Qt::lightGray);
}

void SideBar::mouseMoveEvent(QMouseEvent *event){
    QAction* act = actionAt(event->pos());
    QWidget* actWidget = widgetForAction(act);
    if (act != nullptr){
        qDebug() << "working";
    }
}

