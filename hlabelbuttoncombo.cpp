#include "hlabelbuttoncombo.hh"

HLabelButtonCombo::HLabelButtonCombo(QString programName, QString btnTxt, QWidget* parent) :
    QWidget(parent), programName_(programName)
{
    QLabel* label = new QLabel(programName, this);
    QPushButton* button = new QPushButton(btnTxt, this);
    QHBoxLayout* pairLayout = new QHBoxLayout;
    this->setLayout(pairLayout);
    pairLayout->addWidget(label);
    pairLayout->addSpacing(100);
    pairLayout->addWidget(button);

    connect(button, &QPushButton::clicked, this,&HLabelButtonCombo::buttonClicked);
}

void HLabelButtonCombo::buttonClicked()
{
    emit clicked(this);
}

QString HLabelButtonCombo::getProgramName()
{
    return programName_;
}
