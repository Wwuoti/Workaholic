#include "statisticPage.hh"

statisticPage::statisticPage(QWidget *parent)
    : QWidget(parent){
    QString path = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) 
        + "/Workaholic/tracked.db";
    qDebug() << path;
    statisticDb_ = new DbManager(path, "programPage");
    
    QStringList weekList;
    QStringList yearList;
    QDate currentDate = QDate::currentDate();
    //week numbers can be off a little bit, so 30 doesn't always work
    QDate maxDate = QDate(currentDate.year(), 12, 29);
    for (int i = 1; i < maxDate.weekNumber(); ++i){
       weekList.append(QString::number(i));
    }
    for (int i = 2000; i <= maxDate.year(); ++i){
       yearList.append(QString::number(i));
    }
    //qDebug() << currentDate.weekNumber() << maxDate.weekNumber();

    graphLayout_ = new QVBoxLayout(this);
    QHBoxLayout* selectorLayout = new QHBoxLayout();
    graphLayout_->addLayout(selectorLayout);

    weekSelector_ = new QComboBox(this);
    yearSelector_ = new QComboBox(this);
    weekSelector_->addItems(weekList);
    weekSelector_->setCurrentIndex(currentDate.weekNumber()-1);
    selectorLayout->addWidget(new QLabel("Select week:"));
    selectorLayout->addWidget(weekSelector_);

    yearSelector_->addItems(yearList);
    selectorLayout->addWidget(new QLabel("Select year:"));
    selectorLayout->addWidget(yearSelector_);
    yearSelector_->setCurrentIndex(yearList.size()-1);

    //selectorLayout->addSpacing(500);
    connect(weekSelector_, &QComboBox::currentTextChanged, this, &statisticPage::weekChanged);
    connect(this, &statisticPage::newWeek, this, &statisticPage::addWeekTable);
    connect(yearSelector_, &QComboBox::currentTextChanged, this, &statisticPage::yearChanged);
    //updateGraph(QDate::currentDate().toString());
    weekTable_ = new QTableWidget(this);
    graphLayout_->addWidget(weekTable_);
    this->addWeekTable(beginningOfWeek(weekSelector_->currentText().toInt()));
    connect(weekTable_, &QTableWidget::itemDoubleClicked, this, &statisticPage::doubleClick);
}

statisticPage::~statisticPage(){

}

void statisticPage::updateGraph(const QString dateString_){
    QWidget* graphWindow = new QWidget();
    graphWindow->setWindowFlag(Qt::Dialog);
    QFrame* graphFrame = new QFrame(graphWindow);
    QVBoxLayout* windowLayout = new QVBoxLayout(graphFrame);
    chartscroller_ = new QScrollArea;

    std::map<QString, std::map<int, double>> stats = statisticDb_->fetchStats(dateString_);
    //categories are the vertical bars themselves
    QStringList categories;
    std::vector<QBarSet*> setList; 
    for (const auto& program :stats){
        QBarSet *set = new QBarSet(program.first);
        std::map<int, double> hourMap = program.second;
        for(int i = 0; i < 24; ++i){
            //qDebug() << "Program" << program.first << "usage percentage at hour"
            //<< map.first << "is" << map.second*100 << "%";
            //sets are percentages of productivity
            if(hourMap.find(i) == hourMap.end()){
                *set << 0;
            }else{
                *set << hourMap[i];
            }
            
            //add bars for each tracked hour
            categories << QString::number(i)+":00";

        }
        setList.push_back(set);
    }

    QPercentBarSeries* series = new QPercentBarSeries();
    for (auto set : setList){
        series->append(set);
    }
    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("Current productivity");
    chart->setAnimationOptions(QChart::SeriesAnimations);

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);
    chart->setAxisX(axis, series);
    chart->setMinimumWidth(1500);
    chart->setMinimumHeight(chartscroller_->height()-20);
    QSizePolicy chartsize = chart->sizePolicy();
    chartsize.setVerticalPolicy(QSizePolicy::Maximum);
    chart->setSizePolicy(chartsize);

    //show the names of programs on the bottom
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    chartView_ = new QChartView(chart);
    chartView_->setRenderHint(QPainter::Antialiasing);

    //windowLayout->addWidget(chartView_);
    chartscroller_->setWidget(chartView_);
    windowLayout->addWidget(chartscroller_);
    graphWindow->resize(QDesktopWidget().availableGeometry(this).size() * 0.7);

    graphFrame->resize(graphWindow->size());
    chartView_->resize(graphWindow->size()-QSize(40,40));
    graphWindow->show();
}

void statisticPage::addWeekTable(QDate date){
    weekTable_->clear();
    weekTable_->setRowCount(24);
    weekTable_->setColumnCount(7);
    //Iterate through dates and insert dates as columns and hours as rows
    //Connect click on date to show updateGraph
    //Show total productivity as a per-program-percentage, and maybe add cell higlight color.

    for(int i = 0; i < 24; ++i){
      QTableWidgetItem* item = new QTableWidgetItem(QString::number(i)+ ":00");
      weekTable_->setVerticalHeaderItem(i, item);
    }
    while (date.dayOfWeek() <= 7) {
        int dateInt = date.dayOfWeek();
        weekTable_->setHorizontalHeaderItem(dateInt-1,
            new QTableWidgetItem(date.toString("ddd d.MM")));
        weekTable_->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        std::map<QString, std::map<int, double>> stats =
            statisticDb_->fetchStats(date.toString("_dd_MM_yyyy"));
        for (const auto& program :stats){
            if (program.first != "Unproductive"){
                std::map<int, double> hourMap = program.second;
                for(int j = 0; j < 24; ++j){
                    double productivity = hourMap[j];
                    QTableWidgetItem* currentItem = weekTable_->item(j, dateInt-1);
                    if (currentItem == 0 && productivity != 0.0){
                        weekTable_->setItem(j,dateInt-1,
                            new QTableWidgetItem(program.first + ": " + QString::number(productivity,10,2)));
                    }else if (currentItem != 0 && productivity != 0.0) {
                        if (currentItem->text().split(": ")[1].toDouble() < hourMap[j]){
                            currentItem->setText(program.first + ": " + QString::number(productivity,10,2));
                        }
                    }
                }
            }
        }
        date = date.addDays(1);
        if (dateInt == 7){
            break;

        }
    }
    weekTable_->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

QDate statisticPage::beginningOfWeek(int weekNumber, int year){
    QDate date = QDate(year, 1, 1);
    while (date.weekNumber() < weekNumber){
        date = date.addDays(7);
    }
    //scroll back to Monday
    while (date.dayOfWeek() > 1){
        date = date.addDays(-1);
    }
    return date;
}

void statisticPage::weekChanged(QString week){
    int year = yearSelector_->currentText().toInt();
    QDate newDate = beginningOfWeek(week.toInt(), year);
    emit newWeek(newDate);
}

void statisticPage::yearChanged(QString year){
    int week = weekSelector_->currentText().toInt();
    QDate newDate = beginningOfWeek(week, year.toInt());
    emit newWeek(newDate);
}

void statisticPage::doubleClick(QTableWidgetItem *item){
    QTableWidgetItem* headerItem = weekTable_->horizontalHeaderItem(item->column());
    QString headerText = headerItem->text();
    QStringList splitHeader = headerText.split(' ')[1].split('.');
    int date = splitHeader[0].toInt();
    int month = splitHeader[1].toInt();
    int year = yearSelector_->currentText().toInt();
    //QDate clickedDate = QDate::fromString(headerText, "ddd d.MM");
    QDate clickedDate = QDate(year, month, date);
    qDebug() << clickedDate << headerText;
    updateGraph(clickedDate.toString("_dd_MM_yyyy"));
}
