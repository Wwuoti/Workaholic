#ifndef TRACKERTHREAD_HH
#define TRACKERTHREAD_HH 

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QStandardPaths>

#include "LinuxProcessView.hh"
#include "DbManager.hh"
#include <QSet>
#include <QTimer>
#include <sqlite3.h>
#include <iostream>
class TrackerThread : public QThread{
    Q_OBJECT

public: 
    TrackerThread(QObject *parent = 0);
    ~TrackerThread();
    //execs linux command and returns its output
    std::string exec (const char* cmd);
    //updates currentWindow_
    void trackWindow();

public slots:
    void addNewTrackable(QString);
    void removeTrackable(QString);

signals:
    void newTrackable(const QString& trackable);
    void deleteTrackable(const QString& trackable);
    void PIDMapChanged();
    void trackedProgramsOnStart(const QSet<QString> programs);

protected:
    //handles the execution of the thread
    void run() override;

private:
    LinuxProcessView* ProcessView_;
   
    //TODO: add database to store added programs from mainwindow
    DbManager* trackerDb_;
    QSet<QString> trackedPrograms_;
    //used for ignoring non-added programs 
    bool istracked_ = false;
    //TODO: use these for something, race conditions expected to appear when adding
    //settings saving and database handling 
    QMutex mutex;
    QWaitCondition condition;
    
    //PID of currently active windows
    int currentWindowPID_ = 0;
     Display* maindisp_;


    QTimer* timer_;
};


#endif
