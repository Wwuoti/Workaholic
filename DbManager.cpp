#include "DbManager.hh"

DbManager::DbManager(const QString& path, const QString& connection=""){
    time_ = new QTime;
    dateString_ = QDate::currentDate().toString("_dd_MM_yyyy");
    //hardcode to desired test date
    //dateString_ = "_07_08_2018";
    connectionName_ = connection;
    database_ = QSqlDatabase::addDatabase("QSQLITE", connectionName_);
    database_.setDatabaseName(path);

    if (!database_.open()){
        qDebug() << "Connection to database failed";
        
    }else{
        qDebug() << "Database: connection ok";
        QSqlQuery query(database_);
        query.exec("CREATE TABLE " + dateString_ 
                + " (program text, time text)");
        query.exec("CREATE TABLE savedPrograms (program text)");
    }
}

bool DbManager::addTimestamp(const QString& program){
    bool success = false;
    QString timestamp = time_->currentTime().toString();
    QSqlQuery query(database_);
    query.prepare("INSERT INTO " +dateString_ + "(program, time) VALUES (:program, :timestamp)");
    
    query.bindValue(":program", program);
    query.bindValue(":timestamp", timestamp);
    if(query.exec()){
        success = true;
    }else{
        qDebug() << "addTimestamp error:" 
            << query.lastError();
    }
    return success;

}

bool DbManager::saveTrackable(const QString& program){
    qDebug() << "Saving trackable"; 
    bool success = false;
    QSqlQuery query(database_);
    query.prepare("INSERT INTO savedPrograms(program) VALUES (:program)");
    
    query.bindValue(":program", program);
    if(query.exec()){
        success = true;
        qDebug() << "saveTrackable insert success";
    }else{
        qDebug() << "saveTrackeable error:" 
            << query.lastError();
    }
    return success;

}

bool DbManager::removeTrackable(const QString &program)
{
    qDebug() << "Removing trackable";
    bool success = false;
    QSqlQuery query(database_);
    query.prepare("DELETE FROM savedPrograms WHERE program=(:program)");
    query.bindValue(":program", program);
    if (query.exec()){
        success = true;
        qDebug() << "removeTrackable remove success";
    }
    else {
        qDebug() << "removeTrackable error";
    }
    return success;
}

QSet<QString> DbManager::loadTrackables(){
    QSet<QString> trackedPrograms;
    QSqlQuery query(database_);
    if(!query.exec("SELECT program FROM savedPrograms")){
        qDebug() << "loadTrackables query error!";
    }
    while(query.next()){
        QString programName = query.value(0).toString();
        trackedPrograms.insert(programName); 
    }
    return trackedPrograms;
}

std::map<QString, std::map<int, double>> DbManager::fetchStats(const QString fetchDate){
    //TODO:Use QTime::hour() sort through timestamps
    //the program name is the key, and time spent on that program (in seconds)
    //is the value. Comparison is done with two QTimes and QTime::secsTo()
    //
    dateString_ = fetchDate;
    QSqlQuery query(database_);
    std::map<QString, std::map<int, double>> stats;
    QTime minHour;
    QTime maxHour;
    int hour;
    QTime prevTime;
    QString prevName = "No tracking data";
    if(!query.exec("SELECT * FROM " +dateString_)){
        //qDebug() << "fetchStats query error!";
    }
    while(query.next()){
    //TODO: simplify this mess
    //gives incorrect results if overTime's name is NULL
        QString programName = query.value(0).toString();
        if (programName == NULL){
            programName = "Unproductive";
        }
        if(stats.find(programName) == stats.end()){
            std::map<int, double> hourMap;
            stats.insert({programName, hourMap});
        }
        QTime time = query.value(1).toTime();
        hour = time.hour();
        if (minHour.isNull() || minHour.hour() != hour){
            minHour.setHMS(time.hour(), 0, 0);
        }
        if (maxHour.isNull()){
            maxHour = minHour.addSecs(3600);
        }
        //remember, secsTo() only returns ints 
        double elapsed = (prevTime.secsTo(time));
        if (prevTime.isNull()){
            prevName = "Unproductive";
            elapsed = minHour.secsTo(time);
        }
        //remember, 23:00 is larger than midnight
        if (time > maxHour && maxHour.hour() != 0){
            elapsed = prevTime.secsTo(maxHour);
            QTime overTime; 
            overTime.setHMS(time.hour(), 0, 0);
            double overElapsed = overTime.secsTo(time);
            stats[prevName][hour] += overElapsed/3600;
            //qDebug() << "Overelapsed for" << prevName << "is" << overElapsed << "for hour" << hour;
            hour = maxHour.hour()-1;
            //qDebug() << "time is " << time << "maxhour is " << maxHour;
            //qDebug() << "time exceeds maxHour with value " << elapsed;
        }
        maxHour.setHMS(time.hour()+1, 0, 0);
        //qDebug() << "Elapsed time for program" <<prevName << "is" <<  elapsed   << "measured at  hour" << hour;
        stats[prevName][hour] += elapsed/3600;
        prevTime = time;
        prevName = programName;
        
    }
    //qDebug() << "maxhour:" << maxHour << "prevName:" << prevName << "prevTime:" << prevTime;
    double lastElapsed = prevTime.secsTo(maxHour);
    stats[prevName][prevTime.hour()] += lastElapsed/3600;
    return stats;
}

QStringList DbManager::getTableNames(){
    QStringList tableNames = database_.tables();
    tableNames.removeOne("timestamp");
    tableNames.removeOne("savedPrograms");
    //change names te a proper date format from underscores used in the table names  
    //for (auto &name : tableNames){
    //    QString namestring = name;
    //    QDate date = QDate::fromString(namestring, "_dd_MM_yyyy");
    //    name = date.toString();
    //}
    return tableNames;
}


