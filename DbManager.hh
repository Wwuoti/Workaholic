#ifndef DBMANAGER_HH
#define DBMANAGER_HH

#include <iostream>
#include <QSet>
#include <QObject>
#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QTime>
#include <QDate>
#include <QStandardPaths>

class DbManager : public QObject{
    Q_OBJECT
public: 
    DbManager(const QString& path, const QString& connectionName);
    bool addTimestamp(const QString& program);
    
    QSet<QString> loadTrackables();
    std::map<QString, std::map<int, double>> fetchStats(const QString fetchDate);
    QStringList getTableNames();
public slots:
    bool saveTrackable(const QString& program);
    bool removeTrackable(const QString& program);
private:
    QSqlDatabase database_;
    QTime* time_;
    QString connectionName_;
    QString dateString_;
};

#endif
