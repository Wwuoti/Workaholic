#include "labelButtonWidget.hh"

labelButtonWidget::labelButtonWidget(QString btnTxt, QWidget *parent) : QWidget(parent)
{
    mainLayout_ = new QVBoxLayout();
    mainLayout_->setAlignment(Qt::AlignTop);
    mainLayout_->setSizeConstraint(QLayout::SetMinAndMaxSize);
    setLayout(mainLayout_);
    btnTxt_ = btnTxt;
}

void labelButtonWidget::removeProgram(HLabelButtonCombo* widget)
{
    emit programRemoved(widget->getProgramName());
    widget->deleteLater();
}

void labelButtonWidget::addProgram(QString programName, int index)
{
    HLabelButtonCombo* labelBtnWidget = new HLabelButtonCombo(programName, btnTxt_, this);
    mainLayout_->insertWidget(index, labelBtnWidget);

    connect(labelBtnWidget, &HLabelButtonCombo::clicked, this, &labelButtonWidget::removeProgram);
}


