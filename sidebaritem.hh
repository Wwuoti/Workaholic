#ifndef SIDEBARITEM_HH
#define SIDEBARITEM_HH
#include <QEvent>
#include <QPropertyAnimation>
#include <QToolButton>

class SidebarItem : public QToolButton
{
Q_OBJECT

public:
    SidebarItem(QWidget* parent,
                const QString iconPath, const QString actionName);
//protected:
//    bool event(QEvent* event);
};

#endif // SIDEBARITEM_HH
