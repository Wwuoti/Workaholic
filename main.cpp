#include "mainwindow.hh"
#include <QApplication>

int main(int argc, char *argv[]){
    qRegisterMetaType<QSet<QString>>("QSet");
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
