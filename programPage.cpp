#include "programPage.hh"

#include <QMessageBox>
#include <algorithm>

programPage::programPage(QWidget *parent)
    : QWidget(parent){
    setupPage();
}
programPage::~programPage(){
}

void programPage::setupPage(){

    QHBoxLayout* listLayout = new QHBoxLayout;
    
    //add a label and list to select desired tracked program 
    listLayout->addWidget(new QLabel("Select tracked program:",this)); 
   
    //add a combo box to select desired process
    //TODO: Possible to change into an input menu with fuzzy search?
    processBox_ = new QComboBox();
    listLayout->addWidget(processBox_); 
    
    //add a button to confirm process selection from list
    AddButton_ = new QPushButton("Add", this);
    listLayout->addWidget(AddButton_);
    
    //add the process list 
    LinuxProcesses_ = new LinuxProcessView;
    updateProcessBox();

    mainLayout_ = new QVBoxLayout;
    mainLayout_->addLayout(listLayout);

    programWidget_ = new labelButtonWidget("Remove", this);

    QScrollArea* programScrollArea = new QScrollArea();
    programScrollArea->setWidget(programWidget_);
    mainLayout_->addWidget(programScrollArea);

    this->setLayout(mainLayout_);

    connect(AddButton_, &QPushButton::clicked, this, &programPage::sendTrackable);
}


void programPage::sendTrackable(){
    QString programName = processBox_->currentText();
    for (auto program : trackedPrograms_){
        if (program == programName){
            QMessageBox msgBox;
            msgBox.setText("Program '" + programName + "' has already been added to tracked programs");
            msgBox.exec();
            return;
        }
    }
    emit newTrackable(programName);
    addProgramLabel(programName);

}

void programPage::addProgramLabel(QString programName)
{
    trackedPrograms_.push_back(programName);
    std::sort(trackedPrograms_.begin(), trackedPrograms_.end());
    int i;
    for (i = 0; i < trackedPrograms_.length(); ++i){
        if (trackedPrograms_[i] == programName) {
            break;
        }
    }
    programWidget_->addProgram(programName, i);
    connect(programWidget_, &labelButtonWidget::programRemoved, this, &programPage::removeProgram);
}

void programPage::removeProgram(QString programName)
{
    emit removeTrackable(programName);
    trackedPrograms_.removeOne(programName);
}

void programPage::addProgramsOnStart(const QSet<QString> programs)
{
    for (auto program : programs){
        addProgramLabel(program);
    }
}


void programPage::updateProcessBox(){
    //populate the process selector box and sort it alphabetically
    QProcessList_ = LinuxProcesses_->getProcessList();
    //processBox_->insertItems(0, QProcessList_);
    for (int i=0; i < QProcessList_.size(); ++i){
        QString item = QProcessList_[i];
        if(processBox_->findText(item) == -1 && item!= "summerProject"){
            processBox_->insertItem(i, item);
        }
    }
    QProcessList_.sort();
}
