#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>

#include <QGridLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QThread>
#include <QListWidget>
#include <QToolBar>
#include <QStackedWidget>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QStandardPaths>

#include "LinuxProcessView.hh"
#include "TrackerThread.hh"
#include "programPage.hh"
#include "statisticPage.hh"
#include "sidebar.hh"
#include "sidebaritem.hh"
class MainWindow : public QMainWindow{
    Q_OBJECT 

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setupLayout();
    QString runPath();
public slots:
    void toTray();
    void fromTray();
private:
    
    SideBar* sidebar_;
    QStackedWidget* navstack_;
    
    QSystemTrayIcon* trayIcon_;
    //separate thread to run tracking on
    TrackerThread* trackerThread_;
    QString programPath_ = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/summerproject";
    QList<QAction*> actionList_;

    void changeNavstackIndex(QAction *action);
} ;
#endif //MAINWINDOW_HH

