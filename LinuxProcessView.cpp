#include "LinuxProcessView.hh"
LinuxProcessView::LinuxProcessView(){
    maindisp_ = XOpenDisplay(nullptr);
    listProcesses(); 
}

LinuxProcessView::~LinuxProcessView(){
    XCloseDisplay(maindisp_);
}

void LinuxProcessView::getWindowAtom(const char* windowAtom,
        Display* display, Window window, 
        unsigned long &numItems, long*& array){
    //Define the desired window property, (foreground window, 
    //PID, window name...)
    Atom a = XInternAtom(display, windowAtom , true);
    //Xlib functions always require these variables, even if 
    //you don't need them 
    Atom actualType;
    int format;
    unsigned long bytesAfter;
    unsigned char *data = nullptr; //the actual data we want
    XGetWindowProperty(display, 
                        window, 
                        a, //the defined atom
                        0L,
                        (~0L),
                        //if atom doesn't exist, don't create it
                        false, 
                        //No requirements for atom type
                        AnyPropertyType,
                        //returns the type
                        &actualType, 
                        //the format we want the data in
                        &format,
                        //the number of items the return contains
                        &numItems,
                        //in case of a partial read, returns the number
                        //of bytes remaining
                        &bytesAfter,
                        //the data itself
                        &data);

    // cast to proper format:
    array = (long*) data;
    data = nullptr;
    delete data;
    //std::cout << &data << std::endl;
}

bool LinuxProcessView::isActiveWindow(int givenPID){
    Window rootWindow = DefaultRootWindow(maindisp_); 
    unsigned long numItems;
    //get the list of open windows
    long *clientArr;
    getWindowAtom("_NET_CLIENT_LIST", 
            maindisp_, rootWindow, numItems, clientArr);
    //get the PIDs of each open window
    for (unsigned long k = 0; k < numItems; ++k){
        unsigned long PIDItems;
        long *PID_arr;
        getWindowAtom("_NET_WM_PID", 
            maindisp_, clientArr[k], PIDItems, PID_arr);
        if(PID_arr == nullptr){
            XFree(PID_arr);
            //continue;
        }else{
            int newWindowPID = (int) PID_arr[0];
            if (givenPID == newWindowPID){
                XFree(PID_arr);
                XFree(clientArr);
                return true; 
            }
            XFree(PID_arr);
        }
    }
        XFree(clientArr);
        return false; 
}

bool LinuxProcessView::isUserActive(){
    XScreenSaverInfo *info = XScreenSaverAllocInfo();
    XScreenSaverQueryInfo(maindisp_, DefaultRootWindow(maindisp_), info);
    //idle time is in ms, 1 minute should be good 
    if (info->idle >= 60*1000){
        XFree(info);
        return false; 
    }else{
        XFree(info);
        return true;
    }
}

//Fetches PIDs and startup commands from /proc/ 
void LinuxProcessView::listProcesses(){

    std::map <int, std::string> processList;
     
    DIR           *dirp;
    struct dirent *directory;

    dirp = opendir("/proc/");
    if (dirp){
        while ((directory = readdir(dirp)) != nullptr){
            std::string pid = directory->d_name;
            //look account files with digits in them, ignoring
            //everything else but processes in the /pid folder
            if(isdigit((pid[0])) && isActiveWindow(stoi(pid))){

                //define the filepath
                std::string filename = "/proc/" + pid + "/comm";
                
                //read the file 'comm' containing the command name
                std::ifstream commandFile;
                commandFile.open(filename);
                std::string cmdline;
                getline(commandFile, cmdline);
                processList_.insert({stoi(pid), cmdline});
                commandFile.close();
            }
        }
        closedir(dirp);
    }
}

void LinuxProcessView::printProcesses(){
    for (auto process : processList_){
        std::cout<< "process pid " << process.first << 
          " started with command " << process.second << std::endl;
    }
    
    
}
//makes QStringList from processList_ and returns it
//Used to convert the list into a usable form in the gui
QStringList LinuxProcessView::getProcessList(){
    listProcesses();
    QStringList QProcessList; 
    for (auto process : processList_){
        QProcessList.push_back(QString::fromStdString(process.second));
    
    }
    return QProcessList;
}
std::map<int, std::string> LinuxProcessView::getPIDMap(){
    listProcesses();
    return processList_;
}


